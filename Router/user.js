const express = require('express')
const user = require('../Model/user')
const router = express.Router()

router.post('/login', (req, res) => {
    user.findOne({ email: req.body.email, password: req.body.password }).then(data => {
        if (data === null) {
            res.status(401).json({
                message: "authentication failed"
            })
        } else {
            res.status(200).json({
                id: data._id,
                email: data.email
            })
        }
    })
})

router.post('/signup', (req, res) => {
    user.create(req.body).then(data => {
        console.log(data)
    })
})

module.exports = router


