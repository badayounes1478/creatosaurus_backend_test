const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')

const app = express()

mongoose.connect('mongodb://localhost:27017/creatosaurus', {useNewUrlParser: true, useUnifiedTopology:true});
app.use(cors())
app.use(express.json())
app.use('/user',require('./Router/user'))

app.listen(4000 || process.env.PORT, ()=>{
    console.log("listening on port 4000")
})